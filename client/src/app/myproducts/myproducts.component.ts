import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {AppConfigService} from "../config/app-config.service";

@Component({
  selector: 'app-myproducts',
  templateUrl: './myproducts.component.html',
  styleUrls: ['./myproducts.component.css']
})
export class MyproductsComponent implements OnInit {
  photoUrl;
  products;

  constructor(private productService: ProductService, private appConfig: AppConfigService) {
    this.photoUrl = appConfig.photoUrl;
    productService.GetMyProducts().subscribe(
      data => {
        this.products = data;
        console.log("MY PRODUCTS", data);
      }, error => {
        console.log(error);
      })
  }

  ngOnInit() {
  }



}
