import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from './app.component';
import { RoutingModule } from "./app-routing.module";
import { HomeComponent } from './home/home.component';
import { AuthFilterService } from "./services/security/auth-filter.service";
import { AuthService } from "./services/security/auth.service";
import { ApiService } from "./services/api.service";
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductComponent } from './product/product.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AppConfigService } from "./config/app-config.service";
import { ProductDetileComponent } from './product/product-detile/product-detile.component';
import { ProfileComponent } from './profile/profile.component';
import { UserService } from "./services/user.service";
import { AddProductComponent } from './product/add-product/add-product.component';
import {ProductService} from "./services/product.service";
import {FileDropDirective, FileSelectDirective} from "ng2-file-upload";
import { PayboxComponent } from './paybox/paybox.component';
import { MyproductsComponent } from './myproducts/myproducts.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidebarComponent,
    NavbarComponent,
    ProductComponent,
    NotfoundComponent,
    ProductDetileComponent,
    ProfileComponent,
    AddProductComponent,
    FileSelectDirective,
    FileDropDirective,
    PayboxComponent,
    MyproductsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RoutingModule
  ],
  providers: [AuthFilterService, AppConfigService, ApiService, AuthService, UserService, ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
