import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import {Http, RequestOptions, Headers, URLSearchParams, Response} from "@angular/http";
import {AppConfigService} from "../config/app-config.service";

@Injectable()
export class ApiService {

  private user = null;

  private resourceUrl = '';

  private requestOptions: RequestOptions = null;

  private headers: Headers = null;

  private searchParams: URLSearchParams = null;

  constructor(private http: Http, private appConfig: AppConfigService) { }

  get(url: string, params: any): Observable<any> {
    this.refresh(params);
    return this.http.get(this.resourceUrl + url, this.requestOptions).map(this.extractData).catch(this.handleError);
  }

  post(url: string, params: any, body: any) {
    this.refresh(params);
    return this.http.post(this.resourceUrl + url, body, this.requestOptions).map(this.extractData).catch(this.handleError);
  }

  private refresh(params: any) {
    this.resourceUrl = this.appConfig.resourseServerUrl;
    this.headers = new Headers();
    this.searchParams = new URLSearchParams();

    this.headers.append('Content-Type', 'application/json');

    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        this.searchParams.set(key, encodeURIComponent(params[key]));
      }
    }

    var token = "Bearer " + window.localStorage.getItem('access_token');
    if (token) {
      this.headers.append('Authorization', token);
    }

    this.requestOptions = new RequestOptions({
      headers: this.headers,
      search: this.searchParams
    });
  }

  public hasPermission(permissions): boolean {

    if (this.user) {
      if (permissions) {
        for (let i = 0; i < permissions.length; i++) {
          if (permissions[i] == this.user.role) {
            return true;
          }
        }
        return false;
      }
      return true;
    } else {
      return false;
    }
  }

  public setUser(user) {
    this.user = user;
  }

  private extractData(res: Response) {
    return res;
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    if(error.status == 401) {
      this.user = null;
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
