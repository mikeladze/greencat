import { Injectable } from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from "@angular/router";
import {AuthService} from "./auth.service";
import {ApiService} from "../api.service";

@Injectable()
export class AuthFilterService implements CanActivate {

  constructor(private authService: AuthService, private apiService: ApiService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    return this.apiService.hasPermission(['ADMIN']);

  }

}
