import { Injectable } from '@angular/core';
import { ApiService } from "../api.service";
import {Router} from "@angular/router";
import {AppConfigService} from "../../config/app-config.service";
import {Observable} from "rxjs";

@Injectable()
export class AuthService {

  constructor(private api: ApiService, private appConfig: AppConfigService, private router: Router) { }

  hasValidToken(): Observable<any> {
    return this.api.get('/Users/GetAuthUser', {}).map(
      data => {
        return true;
      })
  }

  redirectLoginPage() {
    var authorizationUrl = this.appConfig.authorizationUrl;
    var client_id = this.appConfig.client_id;
    var redirect_uri = this.appConfig.redirect_uri;
    var response_type = "id_token token";
    var scope = "openid profile dataClime";
    var nonce = "N" + Math.random() + "" + Date.now();
    var state = Date.now() + "" + Math.random();

    var url =
      authorizationUrl + "?" +
      "response_type=" + encodeURI(response_type) + "&" +
      "client_id=" + encodeURI(client_id) + "&" +
      "redirect_uri=" + encodeURI(redirect_uri) + "&" +
      "scope=" + encodeURI(scope) + "&" +
      "response_mode" + encodeURI("form_post") + "&" +
      "nonce=" + encodeURI(nonce) + "&" +
      "state=" + encodeURI(state);

    window.location.href = url;
  }

  AuthorizedCallback() {
    var hash = window.location.hash.substr(1);

    var result: any = hash.split('&').reduce(function (result, item) {
      var parts = item.split('=');
      result[parts[0]] = parts[1];
      return result;
    }, {});

    var access_token = "";
    var id_token = "";

    if (!result.error) {
      access_token = result.access_token;
      id_token = result.id_token;

      console.log('access_token - ', access_token);
      console.log('id token - ', id_token);

      window.localStorage.setItem('access_token', access_token);
      window.localStorage.setItem('id_token', id_token);

      return true;
    }
  }

  public loggOff() {
    var logoutAuthorizationUrl = this.appConfig.logoutAuthorizationUrl;

    var id_token_hint = window.localStorage.getItem('id_token');
    var post_logout_redirect_uri = this.appConfig.post_logout_redirect_uri;

    var url =
      logoutAuthorizationUrl + "?" +
      "id_token_hint=" + encodeURI(id_token_hint) + "&" +
      "post_logout_redirect_uri=" + encodeURI(post_logout_redirect_uri);

    window.localStorage.removeItem('access_token');
    window.localStorage.removeItem('id_token');

    window.location.href = url;
  }

}
