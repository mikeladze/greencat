import { Injectable } from '@angular/core';
import { ApiService } from "./api.service";
import {Observable} from "rxjs";

@Injectable()
export class ProductService {

  constructor(private apiService: ApiService) { }

  filterProducts(product): Observable<any> {
    return this.apiService.post('/Product/FilterProducts', {}, product).map(
      data => {
        return data.json();
      }, error => {
        return error;
      })
  }

  addProduct(product): Observable<any> {
    return this.apiService.post('/Product/AddProduct', {}, product).map(
      data => {
        return data;
      }, error => {
        return error;
      })
  }

  deleteProduct(id): Observable<any> {
    return this.apiService.post('/Product/DeleteProduct', {}, {id: id}).map(
      data => {
        return data;
      }, error => {
        return error;
      })
  }

  updateProductView(id): Observable<any> {
    return this.apiService.post('/Product/UpdateProductView', {}, {id: id, view: 1}).map(
      data => {
        return data;
      }, error => {
        return error;
      })
  }

  GetMyProducts(): Observable<any> {
    return this.apiService.post('/Product/GetMyProducts', {}, {}).map(
      data => {
        return data.json();
      }, error => {
        return error;
      })
  }
}
