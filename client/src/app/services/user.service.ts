import { Injectable } from '@angular/core';
import {ApiService} from "./api.service";
import {Observable} from "rxjs";

@Injectable()
export class UserService {

  constructor(private apiService: ApiService) { }

  getAuthUser(): Observable<any> {
    return this.apiService.get('/User/GetAuthUser', {}).map(
      data => {
        return data.json();
      }, error => {
        return error;
      })
  }

  filterUsers(filter): Observable<any> {
    return this.apiService.post('/User/FilterUsers', {}, filter).map(
      data => {
        return data.json();
      }, error => {
        return error;
      });
  }

  AddMoney(filter): Observable<any> {
    return this.apiService.post('/User/AddMoney', {}, filter).map(
      data => {
        return data;
      }, error => {
        return error;
    })
  }

  ByProduct(user): Observable<any> {
    return this.apiService.post('/User/ByProduct', {}, user).map(
      data => {
        return data;
      }, error => {
        return error;
      })
  }

}
