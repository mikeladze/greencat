import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../services/product.service";
import {AppConfigService} from "../../config/app-config.service";
import {ApiService} from "../../services/api.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-product-detile',
  templateUrl: './product-detile.component.html',
  styleUrls: ['./product-detile.component.css']
})
export class ProductDetileComponent implements OnInit {
  photoUrl: any;
  productDetile: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private productServie: ProductService,
              private appConfig: AppConfigService, private apiService: ApiService, private userService: UserService) {
    this.photoUrl = appConfig.photoUrl;
    this.activatedRoute.params.subscribe(params => {
      var id = params['id'];

      productServie.filterProducts({id: id}).subscribe(
        data => {
          console.log('ProductDetile - ', data);
          this.productDetile = data;
        }, error => {
          console.log(error);
        })
    })
  }

  ngOnInit() {
  }

  hasPermission(permissions) {
    return this.apiService.hasPermission(permissions);
  }

  byProduct(id) {
    var user = {
      products: [id]
    };
    this.userService.ByProduct(user).subscribe(
      data => {
        this.router.navigate(['myproducts'])
      }, error => {
        console.log("error in by products - ", error);
      });
  }

}
