import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../services/product.service";
import {AppConfigService} from "../config/app-config.service";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  photoUrl: any;
  products: any;

  constructor(private router: Router, private productService: ProductService, private appConfig: AppConfigService, private apiService: ApiService) {
    this.photoUrl = appConfig.photoUrl;
    this.refresh();
  }

  ngOnInit() {
  }

  hasPermission(permissions) {
    return this.apiService.hasPermission(permissions);
  }

  refresh() {
    this.productService.filterProducts({}).subscribe(
      data => {
        this.products = data;
        console.log(this.products);
      }, error => {
        console.log(error);
      })
  }

  detile(id) {
    this.productService.updateProductView(id).subscribe(data => data, error => error);
    this.router.navigate(['detile', {id: id}]);
  }

  addProduct() {
    this.router.navigate(['addProduct']);
  }

  remove(id) {
    this.productService.deleteProduct(id).subscribe(
      data => {
        console.log('deleted!!', data);
        this.refresh();
      }, error => {
        console.log('error', error);
      })
  }

}
