import { Component, OnInit } from '@angular/core';
import { ProductService } from "../../services/product.service";

import { FileUploader } from "ng2-file-upload";
import {AppConfigService} from "../../config/app-config.service";
import {Router} from "@angular/router";

const URL = "http://localhost:5001/Product/UploadFiles";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  public uploader: FileUploader = new FileUploader({url: URL, authToken: "Bearer " + window.localStorage.getItem('access_token')});

  product = {
    name: '',
    price: '',
    body: '',
    pictureUrls: []
  };

  constructor(private router: Router, private productService: ProductService, private appConfig: AppConfigService) {}

  ngOnInit() {
  }

  addProduct() {
    var list = [];
    this.uploader.getNotUploadedItems().forEach(function (file) {
      list.push(file.some.name);
    });
    this.product.pictureUrls = list;
    this.productService.addProduct(this.product).subscribe(
      data => {
        console.log("addProduct - ", data);
        this.uploader.uploadAll();
        this.router.navigate(['product']);
      }, error => {
        console.log("addProduct ERROR - ", error);
      })
  }

  public hasBaseDropZoneOver:boolean = false;
  public hasAnotherDropZoneOver:boolean = false;

  public fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  }

}
