import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../services/user.service";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private apiService: ApiService) {
    this.activatedRoute.params.subscribe(params => {
      var id = params['id'];
      userService.filterUsers({email: id}).subscribe(
        data => {
          if ( data.length > 0)
            this.user = data[0];
          console.log(this.user);
        }, error => {
          console.log("error in profile ", error)
        })
    })
  }

  hasPermission(permissions) {
    return this.apiService.hasPermission(permissions);
  }

  ngOnInit() {
  }

}
