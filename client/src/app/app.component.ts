import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from "./services/security/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private router: Router, private authService: AuthService) {
    if (window.location.hash) {
      this.authService.AuthorizedCallback();
    }
    history.pushState("", document.title, window.location.pathname + window.location.search);
  }
}
