import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import {AuthService} from "../services/security/auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: ActivatedRoute, private authService: AuthService) {
  }

  ngOnInit() {
  }

}
