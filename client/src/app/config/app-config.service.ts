import { Injectable } from '@angular/core';

@Injectable()
export class AppConfigService {
  /* Resource server URL */
  public resourseServerUrl: string = 'http://localhost:5001';

  /* Auth Server */
  public authorizationUrl: string = 'http://localhost:5000/connect/authorize';
  public client_id: string = 'client';
  public redirect_uri: string = 'http://localhost:4200/';

  /* Logout */
  public logoutAuthorizationUrl: string = 'http://localhost:5000/connect/endsession';
  public post_logout_redirect_uri: string = 'http://localhost:5000/Unauthorized';

  public photoUrl: string = 'http://localhost:5001/images/';

  public fileUploadUrl: string = 'http://localhost:5001/User/UploadFiles';
}
