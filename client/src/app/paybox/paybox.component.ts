import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-paybox',
  templateUrl: './paybox.component.html',
  styleUrls: ['./paybox.component.css']
})
export class PayboxComponent implements OnInit {
  users;

  constructor(private userService: UserService, private apiService: ApiService) {
    userService.filterUsers({}).subscribe(
      data => {
        this.users = data;
        console.log(data);
      }, error => {
        console.log(error);
      })
  }

  ngOnInit() {
  }

  hasPermission(permissions) {
    return this.apiService.hasPermission(permissions);
  }

  isAdmin(admin) {
    return admin == 'ADMIN' ? false : true;
  }

  AddMoney(id, money) {
    this.userService.AddMoney({id: id, money: money}).subscribe(
      data => {
        if (data.status == 200)
          alert("ჩარიცხვა წარმატებით დასრულდა!");
      }, error => {
        console.log(error);
      })
  }

}
