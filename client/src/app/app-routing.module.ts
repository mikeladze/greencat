import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {HomeComponent} from "./home/home.component";
import {AuthFilterService} from "./services/security/auth-filter.service";
import {ProductComponent} from "./product/product.component";
import {NotfoundComponent} from "./notfound/notfound.component";
import {ProductDetileComponent} from "./product/product-detile/product-detile.component";
import {ProfileComponent} from "./profile/profile.component";
import {AddProductComponent} from "./product/add-product/add-product.component";
import {PayboxComponent} from "./paybox/paybox.component";
import {MyproductsComponent} from "./myproducts/myproducts.component";


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }, {
    path: 'profile',
    component: ProfileComponent
  }, {
    path: 'product',
    component: ProductComponent
  }, {
    path: 'addProduct',
    component: AddProductComponent,
    canActivate: [AuthFilterService]
  }, {
    path: 'detile',
    component: ProductDetileComponent
  }, {
    path: 'paybox',
    component: PayboxComponent,
    canActivate: [AuthFilterService]
  }, {
    path: 'myproducts',
    component: MyproductsComponent
  }, {
    path: '**',
    component: NotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class RoutingModule { }
