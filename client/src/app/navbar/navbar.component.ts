import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/security/auth.service";
import {UserService} from "../services/user.service";
import {ApiService} from "../services/api.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user = {};

  constructor(private router: Router, private authService: AuthService, private userServise: UserService, private apiService: ApiService) {
    this.userServise.getAuthUser().subscribe(data => {
      apiService.setUser(data);
      this.user = data;
    }, error => {
      console.log("error in NavbarComponent -> getAuthUser - ", error);
    })
  }

  ngOnInit() {
  }

  hasPermission(permissions) {
    return this.apiService.hasPermission(permissions);
  }

  userProfile(id) {
    this.router.navigate(['/profile', {id: id}]);
  }

  logout() {
    this.authService.loggOff();
  }

  login() {
    this.authService.redirectLoginPage();
  }

  test(id){
    this.userServise.filterUsers(id).subscribe(data => {
      console.log('test function - ', data);
    }, error => {
      console.log("error in filterUsers", error);
    })
  }

}
