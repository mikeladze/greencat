﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using resource.Users;
using resource.Utils;

namespace resource.Products
{
    public class ProductServiceDecorator<T> : Decorator<T>, IProductsService
        where T : IProductsService
    {
        public ProductServiceDecorator(T original) : base(original)
        {
        }

        public virtual Task<List<Product>> FilterProducts(Product product)
            => Original.FilterProducts(product);

        public virtual Task AddProductAsync(Product product)
            => Original.AddProductAsync(product);

        public virtual Task<UpdateResult> UpdateProduct(Product product)
            => Original.UpdateProduct(product);

        public virtual Task<DeleteResult> DeleteProduct(Product product)
            => Original.DeleteProduct(product);
    }
}