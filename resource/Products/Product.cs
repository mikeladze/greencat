﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace resource.Products
{
    public class Product
    {
        [StringLength(24, MinimumLength = 0)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonIgnore]
        public List<string> SearchByIds { get; set; }

        [BsonIgnore]
        public string RemoveUserId { get; set; }

        public string Name { get; set; }

        public string body { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Price not valid!!")]
        public int Price { get; set; }

        public int view { get; set; }

        public List<string> PictureUrls { get; set; }
    }
}