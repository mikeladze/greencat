﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Products
{
    public interface IProductsStore
    {
        Task FilterProducts(FilterDefinition<Product> filter);

        Task AddProductAsync(Product product);

        Task<UpdateResult> UpdateProduct(FilterDefinition<Product> filter, UpdateDefinition<Product> update);

        Task<DeleteResult> DeleteProduct(FilterDefinition<Product> product);
    }
}