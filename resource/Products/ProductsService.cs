﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp;
using MongoDB.Driver;

namespace resource.Products
{
    public class ProductsService : IProductsService
    {
        private readonly IProductsStore _productsStore;

        private readonly ProductUtils _productUtils;

        public ProductsService(IProductsStore productsStore, ProductUtils productUtils)
        {
            _productsStore = productsStore;
            _productUtils = productUtils;
        }

        public Task<List<Product>> FilterProducts(Product product)
        {
            return _productsStore.FilterProducts(_productUtils.GetFilterDefinition(product)) as Task<List<Product>>;
        }

        public async Task AddProductAsync(Product product) => await _productsStore.AddProductAsync(product);

        public async Task<UpdateResult> UpdateProduct(Product product)
            => await _productsStore.UpdateProduct(
                _productUtils.GetFilterDefinition(product),
                _productUtils.GetUpdateDefinition(product));

        public async Task<DeleteResult> DeleteProduct(Product product)
            => await _productsStore.DeleteProduct(_productUtils.GetFilterDefinition(product));
    }
}