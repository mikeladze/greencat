﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;

namespace resource.Products
{
    public class ProductUtils
    {
        public FilterDefinition<Product> GetFilterDefinition(Product product)
        {
            var builder = Builders<Product>.Filter;

            var filterDefinitions = new List<FilterDefinition<Product>>();

            if (!string.IsNullOrEmpty(product?.Id))
            {
                filterDefinitions.Add(builder.Where(f => f.Id == product.Id));
            }

            if (!string.IsNullOrEmpty(product?.Name))
            {
                filterDefinitions.Add(builder.Where(f => f.Name.Contains(product.Name)));
            }

            if (product?.SearchByIds?.Count > 0)
            {
                filterDefinitions.Add(builder.In(f => f.Id, product.SearchByIds));
            }

            return Builders<Product>.Filter.And(filterDefinitions);
        }

        public UpdateDefinition<Product> GetUpdateDefinition(Product product)
        {
            var builder = Builders<Product>.Update;

            var updateDefinitions = new List<UpdateDefinition<Product>> {};

            if (!string.IsNullOrEmpty(product?.Name))
            {
                updateDefinitions.Add(builder.Set(f => f.Name, product.Name));
            }

            if (product?.view == 1)
            {
                updateDefinitions.Add(builder.Inc(f => f.view , 1));
            }

            return Builders<Product>.Update.Combine(updateDefinitions);
        }
    }
}