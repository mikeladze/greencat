﻿using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using resource.Users;

namespace resource.Products
{
    public class DeleteIdFromUserCollectionDecorator<T> : ProductServiceDecorator<T>
        where T : IProductsService
    {
        private readonly IUsersService _usersService;

        public DeleteIdFromUserCollectionDecorator(T original, IUsersService usersService) : base(original)
        {
            _usersService = usersService;
        }

        public override async Task<DeleteResult> DeleteProduct(Product product)
        {
            Task<DeleteResult> result = base.DeleteProduct(product);

            User user = new User();
            user.Id = product.RemoveUserId;
            user.RemoveProductId = product.Id;
            await _usersService.UpdateUser(user);

            return await result;
        }

    }
}