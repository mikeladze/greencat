﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityServer4.Validation;
using MongoDB.Driver;

namespace resource.Products
{
    public interface IProductsService
    {
        Task<List<Product>> FilterProducts(Product product);

        Task AddProductAsync(Product product);

        Task<UpdateResult> UpdateProduct(Product product);

        Task<DeleteResult> DeleteProduct(Product product);
    }
}