﻿using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Products
{
    public class ProductsStore : IProductsStore
    {
        private readonly IMongoCollection<Product> _productsCollection;

        public ProductsStore(IMongoCollection<Product> productsCollection)
        {
            _productsCollection = productsCollection;
        }

        public Task FilterProducts(FilterDefinition<Product> filter)
            => _productsCollection.Find(filter).ToListAsync();


        public async Task AddProductAsync(Product product) => await _productsCollection.InsertOneAsync(product);

        public async Task<UpdateResult> UpdateProduct(FilterDefinition<Product> filter, UpdateDefinition<Product> update)
            => await _productsCollection.UpdateOneAsync(filter, update, new UpdateOptions());

        public async Task<DeleteResult> DeleteProduct(FilterDefinition<Product> filter)
            => await _productsCollection.DeleteOneAsync(filter);
    }
}