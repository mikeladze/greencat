using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace resource.Users
{
    public class User
    {
        [StringLength(24, MinimumLength = 0)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string Email { get; set; }

        public int Money { get; set; }

        public List<string> Products { get; set; }

        [BsonIgnore]
        public Boolean Add { get; set; }

        [BsonIgnore]
        public string RemoveProductId { get; set; }

        public string Role { get; set; }
    }
}