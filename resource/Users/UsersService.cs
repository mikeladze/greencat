﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Users
{
    public class UsersService : IUsersService
    {
        private readonly IUsersStore _usersStore;

        private readonly UserUtils _userUtils;

        public UsersService(IUsersStore usersStore, UserUtils userUtils)
        {
            _usersStore = usersStore;
            _userUtils = userUtils;
        }

        public Task<List<User>> FilterUsers(User user)
        {
            return _usersStore.FilterUsers(_userUtils.GetFilterDefinition(user)) as Task<List<User>>;
        }

        public async Task AddUserAsync(User user)
            => await _usersStore.AddUserAsync(user);


        public Task<UpdateResult> UpdateUser(User user)
            => _usersStore.UpdateUser(_userUtils.GetFilterDefinition(user), _userUtils.GetUpdateDefinition(user));


        public Task<DeleteResult> DeleteUser(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}