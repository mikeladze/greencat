﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Users
{
    public interface IUsersStore
    {
        Task FilterUsers(FilterDefinition<User> user);

        Task AddUserAsync(User user);

        Task<UpdateResult> UpdateUser(FilterDefinition<User> filter, UpdateDefinition<User> update);

        Task<DeleteResult> DeleteUser(User user);
    }
}