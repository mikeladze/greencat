﻿using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Users
{
    public class UsersStore : IUsersStore
    {
        private readonly IMongoCollection<User> _usersCollection;

        public UsersStore(IMongoCollection<User> usersCollection)
        {
            _usersCollection = usersCollection;
        }

        public Task FilterUsers(FilterDefinition<User> filter)
        {
            return _usersCollection.Find(filter).ToListAsync();
        }

        public async Task AddUserAsync(User user)
            => await _usersCollection.InsertOneAsync(user);

        public async Task<UpdateResult> UpdateUser(FilterDefinition<User> filter, UpdateDefinition<User> update)
            => await _usersCollection.UpdateOneAsync(filter, update, new UpdateOptions());


        public Task<DeleteResult> DeleteUser(User user)
        {
            throw new System.NotImplementedException();
        }
    }
}