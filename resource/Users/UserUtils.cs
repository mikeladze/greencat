﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;

namespace resource.Users
{
    public class UserUtils
    {
        public FilterDefinition<User> GetFilterDefinition(User user)
        {
            var builder = Builders<User>.Filter;

            var filterDefinitions = new List<FilterDefinition<User>>();

            if (!string.IsNullOrEmpty(user?.Id))
            {
                filterDefinitions.Add(builder.Where(f => f.Id == user.Id));
            }

            if (!string.IsNullOrEmpty(user?.FirstName))
            {
                filterDefinitions.Add(builder.Where(f => f.FirstName.Contains(user.FirstName)));
            }

            if (!string.IsNullOrEmpty(user?.Email))
            {
                filterDefinitions.Add(builder.Where(f => f.Email.Contains(user.Email)));
            }

            return Builders<User>.Filter.And(filterDefinitions);
        }

        public UpdateDefinition<User> GetUpdateDefinition(User user)
        {
            var builder = Builders<User>.Update;

            var updateDefinitions = new List<UpdateDefinition<User>> {};

            if (!string.IsNullOrEmpty(user?.FirstName))
            {
                updateDefinitions.Add(builder.Set(f => f.FirstName, user.FirstName));
            }

            if (user?.Money > 0 && user?.Add == false)
            {
                updateDefinitions.Add(builder.Inc(f => f.Money, user.Money));
            }

            if (user?.Money > 0 && user?.Add == true)
            {
                updateDefinitions.Add(builder.Set(f => f.Money, user.Money));
            }

            if (user?.Products?.Count > 0)
            {
                updateDefinitions.Add(builder.Push(f => f.Products, user.Products[0]));
            }

            if (user?.RemoveProductId != null)
            {
                updateDefinitions.Add(builder.Pull(f => f.Products, user.RemoveProductId));
            }

            return Builders<User>.Update.Combine(updateDefinitions);
        }
    }
}