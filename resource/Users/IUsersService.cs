using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace resource.Users
{
    public interface IUsersService
    {
        Task<List<User>> FilterUsers(User user);

        Task AddUserAsync(User user);

        Task<UpdateResult> UpdateUser(User user);

        Task<DeleteResult> DeleteUser(User user);
    }
}