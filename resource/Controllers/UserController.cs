using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp;
using MongoDB.Driver;
using NuGet.Common;
using resource.Users;
using resource.Extensions;
using resource.Products;

namespace resource.Controllers
{
    [Authorize]
    public class UserController : Controller
    {

        private readonly IUsersService _usersService;

        private readonly IProductsService _productsService;

        public UserController(IUsersService usersService, IProductsService productsService)
        {
            _usersService = usersService;
            _productsService = productsService;
        }

        [HttpGet]
        public User GetAuthUser()
        {
            User user = User.GenerateUserFromClaims();

            Console.WriteLine("this is - " + user.Id + " " + user.FirstName + " " + user.Email);
            
            user.Id = "";
            user.Money = 0;
            user.Products = new List<string>();

            Task<List<User>> users = _usersService.FilterUsers(user);

            if (!(users.Result.Count > 0))
            {
                _usersService.AddUserAsync(user);
            }

            return user;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<UpdateResult> AddMoney([FromBody] User user)
        {
            user.Add = false;
            return await _usersService.UpdateUser(user);
        }

        [Authorize(Roles = "GUEST")]
        [HttpPost]
        public async Task<Boolean> ByProduct([FromBody] User user)
        {
            Product product = new Product();

            if (user?.Products.Count > 0)
                product.Id = user.Products[0];


            List<Product> productList = await _productsService.FilterProducts(product);

            User authUser = User.GenerateUserFromClaims();
            authUser.Id = "";

            List<User> userList = await _usersService.FilterUsers(authUser);

            if (productList.Count > 0 && userList.Count > 0)
                if (userList[0].Money > productList[0].Price)
                {
                    userList[0].Money -= productList[0].Price;
                    userList[0].Products = new List<string>();
                    userList[0].Products.Add(productList[0].Id);
                    userList[0].Add = true;

                    await _usersService.UpdateUser(userList[0]);
                    return true;
                }

            return false;
        }

        [HttpPost]
        public async Task<List<User>> FilterUsers([FromBody] User user)
            => await _usersService.FilterUsers(user);
    }
}