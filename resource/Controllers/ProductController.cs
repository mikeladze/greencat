﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using resource.Products;
using resource.Users;
using resource.Extensions;

namespace resource.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IHostingEnvironment _environment;

        private readonly IProductsService _productsService;

        private readonly IUsersService _usersService;

        public ProductController(IHostingEnvironment environment, IProductsService productsService, IUsersService usersService)
        {
            _productsService = productsService;
            _environment = environment;
            _usersService = usersService;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<List<Product>> FilterProducts([FromBody] Product product)
            => await _productsService.FilterProducts(product);

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public Task AddProduct([FromBody]Product product)
            => ModelState.IsValid ? _productsService.AddProductAsync(product) : null;

        [HttpPost]
        public async Task<UpdateResult> EditUser([FromBody] Product product)
            => await _productsService.UpdateProduct(product);

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<DeleteResult> DeleteProduct([FromBody] Product product)
        {
            User user = User.GenerateUserFromClaims();
            user.Id = "";

            List<User> users = await _usersService.FilterUsers(user);

            if (users.Count > 0)
            {
                product.RemoveUserId = users[0].Id;
                return await _productsService.DeleteProduct(product);
            }
            return null;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<UpdateResult> UpdateProductView([FromBody] Product product)
        {
            return await _productsService.UpdateProduct(product);
        }

        [HttpPost]
        public async Task<List<Product>> GetMyProducts()
        {
            User user = User.GenerateUserFromClaims();
            user.Id = "";

            List<User> users = await _usersService.FilterUsers(user);

            if (users.Count > 0 && users[0].Products.Count > 0)
            {
                Product product = new Product();
                product.SearchByIds = users[0].Products;
                return await _productsService.FilterProducts(product);
            }

            return null;
        }

        [Authorize(Roles = "ADMIN")]
        [HttpPost]
        public async Task<bool> UploadFiles(IFormFile file)
        {
            var uploads = Path.Combine(_environment.WebRootPath, "images");

            if (file.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            return true;
        }
    }
}