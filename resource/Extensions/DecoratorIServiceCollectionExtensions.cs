﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace resource.Extensions
{
    public static class DecoratorIServiceCollectionExtensions
    {
        public static IServiceCollection AddScoped(this IServiceCollection services, Type serviceType, Type implementationType, IEnumerable<Type> decoratorTypes)
            => AddScoped(services, serviceType, implementationType, decoratorTypes, collection => collection.AddScoped(implementationType));

        public static IServiceCollection AddScoped<TService, TImplementation>(this IServiceCollection services, IEnumerable<Type> decoratorTypes)
            where TService : class
            where TImplementation : class, TService
            => AddScoped(services, typeof(TService), typeof(TImplementation), decoratorTypes, collection => collection.AddScoped(typeof(TImplementation)));

        public static IServiceCollection AddScoped<TService, TImplementation>(this IServiceCollection services, Func<IServiceProvider, TImplementation> implementationFactory,
            IEnumerable<Type> decoratorTypes)
            where TService : class
            where TImplementation : class, TService
            => AddScoped(services, typeof(TService), typeof(TImplementation), decoratorTypes, collection => collection.AddScoped(implementationFactory));

        private static IServiceCollection AddScoped(this IServiceCollection services, Type serviceType, Type implementationType, IEnumerable<Type> decoratorTypes,
            Func<IServiceCollection, IServiceCollection> addImplementationFunc)
        {
            addImplementationFunc(services);

            var genericTypeArgument = implementationType;

            if (decoratorTypes != null)
            {
                foreach (var decoratorType in decoratorTypes.Reverse())
                {
                    var decoratorGenericType = decoratorType.MakeGenericType(genericTypeArgument);

                    services.AddScoped(decoratorGenericType);

                    genericTypeArgument = decoratorGenericType;
                }
            }

            return services.AddScoped(serviceType, genericTypeArgument);
        }
    }
}