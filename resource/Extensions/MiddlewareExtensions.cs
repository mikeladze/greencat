using Microsoft.AspNetCore.Builder;
using resourxe.Middleware;

namespace resource.Extensions
{
    public static class MiddlewareExtensions {
        
        public static IApplicationBuilder UseBearerTokenMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BearerTokenMiddleware>();
        }
    }
}