using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using resource.Products;
using resource.Users;

namespace resource.Extensions
{
    public static class ServicesExtensions
    {
        public static IServiceCollection addMongo(this IServiceCollection services, IConfigurationSection connectionConfiguration)
        {
            return services
                    .AddScoped(provider => new MongoClient(connectionConfiguration["ConnectionString"]) as IMongoClient)
                    .AddScoped(provider => provider.GetService<IMongoClient>().GetDatabase(connectionConfiguration["DataBase"]));
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services) => services

            .AddApplicationUsers()
            .AddApplicationProducts(new [] { typeof(DeleteIdFromUserCollectionDecorator<>) });

        private static IServiceCollection AddApplicationUsers(this IServiceCollection services,
            IEnumerable<Type> serviceDecorators = null)
        {
            services.AddScoped<UserUtils>();
            services.AddScoped(CollectionOf<User>("Users"));
            services.AddScoped<IUsersStore, UsersStore>();
            services.AddScoped<IUsersService, UsersService>();
            return services;
        }

        private static IServiceCollection AddApplicationProducts(this IServiceCollection services,
            IEnumerable<Type> serviceDecorators = null)
        {
            services.AddScoped<ProductUtils>();
            services.AddScoped(CollectionOf<Product>("Products"));
            services.AddScoped<IProductsStore, ProductsStore>();
            services.AddScoped<IProductsService, ProductsService>(serviceDecorators);
            return services;
        }

        private static Func<IServiceProvider, IMongoCollection<T>> CollectionOf<T>(string name)
            => s => s.GetService<IMongoDatabase>().GetCollection<T>(name);
    }
}