using System.Linq;
using System.Security.Claims;
using IdentityModel;
using resource.Users;

namespace resource.Extensions
{
    public static class ClaimsExtensions
    {
        public const string IdClaimType = JwtClaimTypes.Subject;
        
        public static string GetClaim(this ClaimsPrincipal claimsIdentity, string type)
                => claimsIdentity.HasClaim(c => c.Type == type) ? claimsIdentity.FindFirst(type).Value : null;

        public static string GetId(this ClaimsPrincipal claimsPrincipal)
                => claimsPrincipal.HasClaim(c => c.Type == IdClaimType) ? claimsPrincipal.GetClaim(IdClaimType) : null;

        public static User GenerateUserFromClaims(this ClaimsPrincipal claimsIdentity)
                => new User() {
                    Id = claimsIdentity.GetId(),
                    FirstName = claimsIdentity.GetClaim("Firstname"),
                    Email = claimsIdentity.GetClaim("username"),
                    Role = claimsIdentity.GetClaim("role")};
    }
}