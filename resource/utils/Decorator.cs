﻿namespace resource.Utils
{
    public class Decorator<T>
    {
        protected T Original { get;  }

        public Decorator(T original)
        {
            Original = original;
        }
    }
}