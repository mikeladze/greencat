﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace identity.Rights
{
    public interface IRightsStore
    {
        Task<List<Right>> FilterRights(Right right);
    }
}