﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace identity.Rights
{
    public class Right
    {
        [StringLength(24, MinimumLength = 0)]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Key { get; set; }

        [BsonIgnore]
        public List<string> IdList { get; set; }
    }
}