﻿using System;
using System.Collections.Generic;
using MongoDB.Driver;

namespace identity.Rights
{
    public class RightUtils
    {
        public FilterDefinition<Right> GetFilterDefinition(Right right)
        {
            var builder = Builders<Right>.Filter;

            var filterDefinition = new List<FilterDefinition<Right>>();

            if (right?.IdList?.Count > 0)
            {
                filterDefinition.Add(builder.Where(r => right.IdList.Contains(r.Id)));
            }

            return Builders<Right>.Filter.And(filterDefinition);
        }
    }
}