﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace identity.Rights
{
    public class RightsStore : IRightsStore
    {
        private readonly IMongoCollection<Right> _rightCollection;

        private readonly RightUtils _rightUtils;

        public RightsStore(IMongoCollection<Right> rightCollection, RightUtils rightUtils)
        {
            _rightCollection = rightCollection;
            _rightUtils = rightUtils;
        }

        public Task<List<Right>> FilterRights(Right right)
            => _rightCollection.Find(_rightUtils.GetFilterDefinition(right)).ToListAsync();
    }
}