﻿using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using identity.Users;
using IdentityServer4.Validation;

namespace identity.Auth
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IUsersStore _usersStore;

        public ResourceOwnerPasswordValidator(IUsersStore usersStore)
        {
            _usersStore = usersStore;
        }

        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            string username = context.UserName;
            string password = context.Password;

            var user = _usersStore.FilterUsers(new User() {username = username}).Result;

            if (user?.Count == 1)
            {
                if (BCrypt.Net.BCrypt.Verify(password, user[0].Password))
                {
                    context.Result = new GrantValidationResult(new ClaimsPrincipal());
                    return Task.FromResult(context);
                }
                return Task.FromResult(context.Result = null);
            }
            else
            {
                return Task.FromResult(context.Result = null);
            }
        }
    }
}