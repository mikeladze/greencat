﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using identity.Rights;
using identity.Users;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;

namespace identity.Auth
{
    public class ProfileService : IProfileService
    {
        private readonly IUsersStore _usersStore;

        private readonly IRightsStore _rightsStore;

        public ProfileService(IUsersStore usersStore, IRightsStore rightsStore)
        {
            _usersStore = usersStore;
            _rightsStore = rightsStore;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            string authUserId = context.Subject.Claims.ToList().Find(s => s.Type == "sub").Value;
            Console.WriteLine(authUserId);

            User filterUser = new User() { Id = authUserId};
            var users = _usersStore.FilterUsers(filterUser);

            if (users == null || users.Result.Count == 0)
            {
                return Task.FromResult(0);
            }
            else
            {
                var user = users.Result;
                List<Claim> claims = new List<Claim>();

                Console.WriteLine(user[0].Id + " " + user[0].username + " " + user[0].Firstname);

                claims.Add(new Claim(JwtClaimTypes.Subject, user[0].Id));
                claims.Add(new Claim("username", user[0].username));
                claims.Add(new Claim("Firstname", user[0].Firstname));

                var rights = _rightsStore.FilterRights(new Right() {IdList = user[0].Rights}).Result;

                foreach (var right in rights)
                {
                    claims.Add(new Claim(JwtClaimTypes.Role, right.Key));
                }

                context.IssuedClaims = claims;
                return Task.FromResult(0);
            }
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            return Task.FromResult(0);
        }
    }
}













