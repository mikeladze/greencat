using System;
using System.Collections.Generic;
using System.Security.Claims;
using identity.Auth;
using identity.Users;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services.InMemory;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using identity.Extensions;
using IdentityServer4.Services;
using IdentityServer4.Validation;

namespace identity
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMongo(Configuration.GetSection("mongo:Connection"));

            services.AddApplicationServices();

            services.AddMvc();

//            services.AddDeveloperIdentityServer()
//                .AddInMemoryScopes(GetScopes())
//                .AddInMemoryClients(GetClients())
//                .AddInMemoryUsers(GetUsers());

            var builder = services.AddDeveloperIdentityServer();
            builder.AddInMemoryScopes(GetScopes());
            builder.AddInMemoryClients(GetClients());
            builder.Services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
            builder.Services.AddTransient<IProfileService, ProfileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIdentityServer();

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
        }

        public static IEnumerable<Scope> GetScopes()
        {
            return new List<Scope>
            {
                StandardScopes.OpenId,
                StandardScopes.Profile,

                new Scope
                {
                    Name = "dataClime",
                    DisplayName = "Scope for the data event records resource.",
                    Type = ScopeType.Resource,

                    ScopeSecrets = new List<Secret>
                    {
                        new Secret("dataClimeSecret".Sha256())
                    },

                    Claims = new List<ScopeClaim>
                    {
                        new ScopeClaim("name"),
                        new ScopeClaim("email"),
                        new ScopeClaim(JwtClaimTypes.Role)
                    }
                },
                new Scope
                {

                    Name = "api1",
                    Description = "My API"
                }
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "client",
                    AccessTokenType = AccessTokenType.Reference,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,

                    RedirectUris = { "http://localhost:4200/" },

                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:4200/login"
                    },

                    AllowedCorsOrigins = new List<string>
                    {
                        "http://localhost:4200"
                    },

                    AllowedScopes = new List<string> 
                    {
                        StandardScopes.OpenId.Name,
                        StandardScopes.Profile.Name,
                        "dataClime"
                    }

                }
            };
        }

        public static List<InMemoryUser> GetUsers()
        {
            return new List<InMemoryUser>
            {
                new InMemoryUser
                {
                    Subject = "1",
                    Username = "gio",
                    Password = "password",

                    Claims = new Claim[]
                    {
                        new Claim("name", "giorgi kakabadze"),
                        new Claim("email", "kakabadze@email.com"),
                        new Claim(JwtClaimTypes.Role, "ADMIN"),
                    }
                },
                new InMemoryUser
                {
                    Subject = "2",
                    Username = "ucha",
                    Password = "password",

                    Claims = new Claim[]
                    {
                        new Claim("name", "ucha mikeladze"),
                        new Claim("email", "mikeladze@email.com"),
                        new Claim(JwtClaimTypes.Role, "GUEST")
                    }
                }
            };
        }
    }
}
