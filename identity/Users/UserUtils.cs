﻿using System.Collections.Generic;
using MongoDB.Driver;

namespace identity.Users
{
    public class UserUtils
    {
        public FilterDefinition<User> GetFilterDefinition(User user)
        {
            var builder = Builders<User>.Filter;

            var filterDefinitions = new List<FilterDefinition<User>>();

            if (!string.IsNullOrEmpty(user?.Id))
            {
                filterDefinitions.Add(builder.Where(f => f.Id == user.Id));
            }

            if (!string.IsNullOrEmpty(user?.username))
            {
                filterDefinitions.Add(builder.Where(f => f.username == user.username));
            }

            if (!string.IsNullOrEmpty(user?.Firstname))
            {
                filterDefinitions.Add(builder.Where(f => f.Firstname.Contains(user.Firstname)));
            }

            if (!string.IsNullOrEmpty(user?.Email))
            {
                filterDefinitions.Add(builder.Where(f => f.Email.Contains(user.Email)));
            }

            return Builders<User>.Filter.And(filterDefinitions);
        }
    }
}