﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace identity.Users
{
    public interface IUsersStore
    {
        Task<List<User>> FilterUsers(User filter);
    }
}