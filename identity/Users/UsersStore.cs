﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace identity.Users
{
    public class UsersStore : IUsersStore
    {
        private readonly IMongoCollection<User> _userCollection;

        private readonly UserUtils _userUtils;

        public UsersStore(IMongoCollection<User> userCollection, UserUtils userUtils)
        {
            _userCollection = userCollection;
            _userUtils = userUtils;
        }

        public Task<List<User>> FilterUsers(User user)
            => _userCollection.Find(_userUtils.GetFilterDefinition(user)).ToListAsync();
    }
}