using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using identity.Users;
using IdentityServer4.Validation;
using identity.Auth;
using identity.Rights;
using IdentityServer4.Services;

namespace identity.Extensions
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddMongo(this IServiceCollection services, IConfigurationSection connectionConfiguration)
        {
            return services
                    .AddScoped(provider => new MongoClient(connectionConfiguration["ConnectionString"]) as IMongoClient)
                    .AddScoped(provider => provider.GetService<IMongoClient>().GetDatabase(connectionConfiguration["Database"]));
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services) => 
            services
                .AddUsers()
                .AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>()
                .AddTransient<IProfileService, ProfileService>()

                .AddScoped<RightUtils>()
                .AddScoped(CollectionOf<Right>("rights"))
                .AddScoped<IRightsStore, RightsStore>();

        private static IServiceCollection AddUsers(this IServiceCollection services)
        {
            services.AddScoped<UserUtils>();
            services.AddScoped(CollectionOf<User>("users"));
            services.AddScoped<IUsersStore, UsersStore>();
            return services;
        }

        private static Func<IServiceProvider, IMongoCollection<T>> CollectionOf<T>(string name)
            => s => s.GetService<IMongoDatabase>().GetCollection<T>(name);
    }
}